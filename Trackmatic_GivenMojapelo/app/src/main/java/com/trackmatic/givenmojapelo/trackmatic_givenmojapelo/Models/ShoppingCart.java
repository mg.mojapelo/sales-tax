package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Models;

import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Lookup.Constants;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Lookup.GoodType;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class ShoppingCart {

    private List<ShoppingItem> shoppingList = new ArrayList();
    public double salesTax;
    public double totalSales;

    public Observable<List<ShoppingItem>> getShoppingList() {
        return Observable.just(shoppingList);
    }

    public void add(ShoppingItem shoppingItem) {
        shoppingList.add(shoppingItem);
        calculateTotalTaxValue();
    }

    private void calculateTotalTaxValue() {
        totalSales = 0;
        salesTax = 0;
        for (ShoppingItem item : shoppingList) {
            calculateTotalSales(item);
            if (item.isImported()) {
                calculateImportsTaxValue(item);
            }
            if (item.getGoodType() == GoodType.Other) {
                calculateBasicTaxValue(item);
            }
        }
    }

    private void calculateBasicTaxValue(ShoppingItem item) {
       salesTax += ((item.getPrice() * Constants.BASIC_TAX_PERCENTAGE) / 100) * item.getQuantity();
    }

    private void calculateImportsTaxValue(ShoppingItem item) {
        salesTax += ((item.getPrice() * Constants.IMPORT_TAX_PERCENTAGE) / 100) * item.getQuantity();
    }

    private void calculateTotalSales(ShoppingItem item) {
        totalSales += (item.getPrice()) * item.getQuantity();
    }

}
