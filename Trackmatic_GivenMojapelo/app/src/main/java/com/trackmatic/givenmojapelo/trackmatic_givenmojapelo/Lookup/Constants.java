package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Lookup;

public class Constants {

    public static String No_INPUT_MESSAGE = "Please provide input";
    public static double IMPORT_TAX_PERCENTAGE = 5;
    public static double BASIC_TAX_PERCENTAGE = 10;

}
