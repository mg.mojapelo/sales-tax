package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Lookup;

public enum GoodType {
    Food, Other, Medical, Book;
}
