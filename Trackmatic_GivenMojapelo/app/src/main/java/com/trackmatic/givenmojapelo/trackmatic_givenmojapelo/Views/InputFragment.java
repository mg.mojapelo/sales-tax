package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Views;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Lookup.Constants;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Lookup.GoodType;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Models.ShoppingItem;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.R;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.ViewModels.TaxViewModel;
import java.util.Objects;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class InputFragment extends Fragment {

    CompositeDisposable compositeDisposable;
    OnFragmentInteractionListener mListener;
    TaxViewModel viewModel;
    Spinner goodsTypes;
    EditText itemName, itemPrice, itemQuantity;
    TextView taxValue, totalValue;
    FloatingActionButton navigationBtn;
    Button saveItemBtn;
    CheckBox isImported;

    public InputFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_input, container, false);
        goodsTypes = view.findViewById(R.id.itemTypeInputID);
        itemName = view.findViewById(R.id.itemNameInputID);
        isImported = view.findViewById(R.id.itemImportedCheckID);
        itemPrice = view.findViewById(R.id.itemCostInputID);
        itemQuantity = view.findViewById(R.id.itemQuantityInputID);
        navigationBtn = view.findViewById(R.id.navigationBtn);
        saveItemBtn = view.findViewById(R.id.saveItemBtn);
        taxValue = view.findViewById(R.id.livetaxSalesValueID);
        totalValue = view.findViewById(R.id.livetotalSalesValueID);

        viewModel = ViewModelProvider.AndroidViewModelFactory.
                getInstance(Objects.requireNonNull(getActivity()).getApplication()).create(TaxViewModel.class);

        bind();
        updateView();
        onActionListeners();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void updateView() {
        String goodTypesLookup[] = getResources().getStringArray(R.array.goodsTypes);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, goodTypesLookup);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        goodsTypes.setAdapter(dataAdapter);
    }

    private void onActionListeners() {

        navigationBtn.setOnClickListener(view -> {
            OutputFragment fragment = OutputFragment.newInstance(viewModel);
            FragmentTransaction transaction = (getActivity()).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.viewID, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        });

        saveItemBtn.setOnClickListener(view -> saveNewItem());
    }

    private void saveNewItem() {

        if (itemName.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), Constants.No_INPUT_MESSAGE, Toast.LENGTH_SHORT).show();
            itemName.setError("Error");
        } else if (itemPrice.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), Constants.No_INPUT_MESSAGE, Toast.LENGTH_SHORT).show();
            itemPrice.setError("Error");
        } else if (itemQuantity.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), Constants.No_INPUT_MESSAGE, Toast.LENGTH_SHORT).show();
            itemQuantity.setError("Error");
        } else {
            ShoppingItem shoppingItem = new ShoppingItem();
            shoppingItem.setName(itemName.getText().toString());
            shoppingItem.setPrice(Double.parseDouble(itemPrice.getText().toString()));
            shoppingItem.setQuantity(Integer.parseInt(itemQuantity.getText().toString()));
            shoppingItem.setImported(isImported.isChecked());
            shoppingItem.setGoodType(GoodType.valueOf(goodsTypes.getSelectedItem().toString()));
            viewModel.addNewShoppingItem(shoppingItem);

            itemName.getText().clear();
            itemPrice.getText().clear();
            itemQuantity.getText().clear();
        }
    }

    private void bind() {
        compositeDisposable = new CompositeDisposable();

        compositeDisposable.add(viewModel.getTaxObservable().subscribeOn(Schedulers.computation()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(this::setTaxSales));

        compositeDisposable.add(viewModel.getTotalObservable().subscribeOn(Schedulers.computation()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(this::setTotalSales));

    }

    private void setTotalSales(String totalSales) {
        this.totalValue.setText(totalSales);
    }

    private void setTaxSales(String taxSales) {
        this.taxValue.setText(taxSales);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        compositeDisposable.clear();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
