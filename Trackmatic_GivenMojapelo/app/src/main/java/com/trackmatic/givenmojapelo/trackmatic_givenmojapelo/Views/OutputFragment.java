package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Views;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Adapter.ShoppingListAdapter;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Models.ShoppingItem;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.R;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.ViewModels.TaxViewModel;
import java.util.List;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class OutputFragment extends Fragment {

    CompositeDisposable compositeDisposable;
    private OnFragmentInteractionListener mListener;
    TaxViewModel viewModel;
    TextView taxSales, totalSales;
    ListView shoppingListView;
    ShoppingListAdapter adapter;

    public OutputFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        viewModel = (TaxViewModel) bundle.getSerializable("viewModel");
    }


    public static OutputFragment newInstance(TaxViewModel viewModel) {
        OutputFragment fragment = new OutputFragment();
        Bundle args = new Bundle();
        args.putSerializable("viewModel", viewModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_output, container, false);

        taxSales = view.findViewById(R.id.salesTaxID);
        totalSales = view.findViewById(R.id.totalSalesID);
        shoppingListView = view.findViewById(R.id.listID);
        bind();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void bind() {
        compositeDisposable = new CompositeDisposable();

        compositeDisposable.add(viewModel.getShoppingList()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setShoppingList));

        compositeDisposable.add(viewModel.getTotalObservable().subscribeOn(Schedulers.computation()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(this::setTotalSales));

        compositeDisposable.add(viewModel.getTaxObservable().subscribeOn(Schedulers.computation()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(this::setTaxSales));

        viewModel.queryNewData();
    }

    private void setTotalSales(String totalSales) {
        this.totalSales.setText(totalSales);
    }

    private void setTaxSales(String taxSales) {
        this.taxSales.setText(taxSales);
    }

    private void setShoppingList(List<ShoppingItem> shoppingItems) {
        adapter = new ShoppingListAdapter(getContext(), R.layout.item_data_layout, shoppingItems);
        adapter.notifyDataSetChanged();
        shoppingListView.setAdapter(adapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        compositeDisposable.clear();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
