package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Views;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.R;

public class MainActivity extends AppCompatActivity implements InputFragment.OnFragmentInteractionListener,
        OutputFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InputFragment inputFragment = new InputFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.viewID, inputFragment);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
