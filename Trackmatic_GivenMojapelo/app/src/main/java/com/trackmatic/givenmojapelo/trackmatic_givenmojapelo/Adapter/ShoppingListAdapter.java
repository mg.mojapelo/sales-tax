package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Models.ShoppingItem;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.R;

import java.util.List;

public class ShoppingListAdapter extends ArrayAdapter {

    List<ShoppingItem> list;
    Context context;
    int resourceID;

    public ShoppingListAdapter(Context context, int resource, List<ShoppingItem> objects) {
        super(context, resource, objects);

        this.resourceID = resource;
        this.context = context;
        list = objects;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(resourceID, viewGroup, false);
        }

        TextView itemQuantityID = view.findViewById(R.id.itemQuantityID);
        TextView itemNameID = view.findViewById(R.id.itemNameID);
        TextView itemPriceID = view.findViewById(R.id.itemPriceID);

        itemNameID.setText(list.get(i).getName());
        itemQuantityID.setText(Integer.toString(list.get(i).getQuantity()));
        itemPriceID.setText(Double.toString(list.get(i).getPrice()));

        return view;
    }
}
