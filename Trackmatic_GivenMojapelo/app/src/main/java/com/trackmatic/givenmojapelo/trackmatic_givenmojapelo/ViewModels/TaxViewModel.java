package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.ViewModels;

import android.arch.lifecycle.ViewModel;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Models.ShoppingCart;
import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Models.ShoppingItem;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class TaxViewModel extends ViewModel implements Serializable {


    private ShoppingCart shoppingCart = new ShoppingCart();
    private Subject<String> totalObservable = PublishSubject.create();
    private Subject<String> taxObservable = PublishSubject.create();

    public Observable<List<ShoppingItem>> getShoppingList() {
        return shoppingCart.getShoppingList();
    }

    private void updateTax(String tax) {
        taxObservable.onNext(tax);
    }

    private void updateTotalSales(String total) {
        totalObservable.onNext(total);
    }

    public Subject<String> getTotalObservable() {
        return totalObservable;
    }

    public Subject<String> getTaxObservable() {
        return taxObservable;
    }

    public void addNewShoppingItem(ShoppingItem shoppingItem) {
        shoppingCart.add(shoppingItem);
        queryNewData();
    }

    public void queryNewData() {
        DecimalFormat df = new DecimalFormat("###.##");
        updateTotalSales(df.format(shoppingCart.totalSales));
        updateTax(df.format(shoppingCart.salesTax));
    }

}
