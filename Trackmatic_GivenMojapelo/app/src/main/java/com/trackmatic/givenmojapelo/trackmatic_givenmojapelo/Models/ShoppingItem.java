package com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Models;

import com.trackmatic.givenmojapelo.trackmatic_givenmojapelo.Lookup.GoodType;

public class ShoppingItem {

    private double price;
    private String name;
    private int quantity;
    private boolean isImported;
    private GoodType goodType;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    boolean isImported() {
        return isImported;
    }

    public void setImported(boolean imported) {
        isImported = imported;
    }

    GoodType getGoodType() {
        return goodType;
    }

    public void setGoodType(GoodType goodType) {
        this.goodType = goodType;
    }


}
